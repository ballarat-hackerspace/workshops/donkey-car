#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::RevealSlide;
use Mojo::Base 'Mojolicious::Plugin';

use CommonMark qw(:opt);
use Mojo::File qw(path);
use Mojo::Util qw(dumper trim);
use Time::Piece;
use YAML::Tiny;

use constant DEBUG      => $ENV{MOJO_REVEALSLIDE_DEBUG} || 0;

our $VERSION = '1.0';

has conf  => sub { +{} };
has files => sub { +[] };
has posts => sub { +[] };

sub register {
  my ($plugin, $app, $conf) = @_;

  # default values
  $plugin->conf($conf) if $conf;

  $app->log->info(sprintf('Slide processing enabled.'));

  $app->helper('slides.build' => sub {
    my ($self, $path) = @_;

    my $slides = [];

    path('slides/')->list->each(sub {
      my $path = shift;

      my $slide = $plugin->_parse_file($path);

      if ($slide->{group}) {
        if (my ($group) = grep { !!$_->{group_id} && ($_->{group_id} eq $slide->{group}) } @{$slides}) {
          $group->{children} //= [];

          push @{$group->{children}}, $slide;
        } else {
          push @{$slides}, {
            group_id => $slide->{group},
            children => [$slide],
          }
        }
      } else {
        push @{$slides}, $slide;
      }
    });

    return $slides;
  });

  $app->helper('slide.parse' => sub {
    my ($self, $path) = @_;

    my $stash = $plugin->_parse_file($path);

    return $stash;
  });
}

sub _parse_file {
  my ($self, $path) = (shift, shift);

  my %args = @_ > 1 ? @_ : ref $_[0] eq 'HASH' ? %{ $_[0] } : ();

  # ensure the path exists
  return { status => 404, bytes => undef } unless -r $path->to_string;

  # slurp
  my $bytes = $path->slurp;

  # ensure header exists
  return { status => 500, bytes => undef } unless $bytes =~ m/---(.*)---\r?\n/s;

  my $slide = Load($1);
  $bytes =~ s/---.*---\r?\n//s;

  # check if footer/notes exist
  if ($bytes =~ m/=====\r?\n(.*)/s) {
    $slide->{notes} = CommonMark->parse(
      string => trim $1
    )->render_html(OPT_UNSAFE);
    $bytes =~ s/=====\r?\n.*//s;
  }

  # decorate properties
  $slide->{bytes}     = $bytes;
  $slide->{content}   = '';
  $slide->{filename}  = $path->basename;

  $slide->{style}     //= 'markdown';
  $slide->{layout}    //= '';
  $slide->{id}        //= $slide->{filename};

  # split tags
  $slide->{tags} = [ map { trim $_ } split /,/, $slide->{tags} ] if $slide->{tags};

  # store content as appropriate
  $slide->{content} = CommonMark->parse(
    string => trim $bytes,
  )->render_html(OPT_UNSAFE);

  $slide->{status} = 200;

  return $slide;
}

# short circuit grep
sub _sgrep(&@) {
  my $t = shift @_;
  for (@_) { return 1 if &$t };
  return 0;
}

# check for element in array
sub _in_array {
  my $n = shift;
  my @h = ref $_[0] eq 'ARRAY' ? @{$_[0]} : @_;
  return scalar(_sgrep { $n eq $_ } @h);
}

1;

---
group: "ai"
---
### Deep Learning

Deep Learning (DL) is an approach to ML; one that focuses on learning data representations rather than on task-specific algorithms.

Makes use of Deep Neural Networks, which are inspired by the structure and function of the human brain.

Such networks are made of a huge number of layers (hence, their name) where data transforms through multiple layers before producing the output.

DL is used for purposes like Natural Language Processing (NLP), drug discovery and toxicology, bioinformatics, and many more.

=====
=====

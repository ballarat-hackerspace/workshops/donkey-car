---
group: "dk"
---
## Clean Your Model

1. donkey tubclean --tub ~/mycar/data

<div class="full-image">
  <img src="./img/donkey-car-clean-data.png" />
</div>

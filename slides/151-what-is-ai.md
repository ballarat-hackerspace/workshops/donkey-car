---
group: "ai"
---
## What is Artificial Intelligence?

Development in the field of technology has enhanced exponentially over the years. Over this time, we get terms like:

- Artificial Intelligence
- Machine Learning
- Deep Learning

These terms are often confused; conflated or defined similarly.

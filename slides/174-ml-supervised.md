---
group: "ml"
---
## Supervised Learning

Supervised learning typically begins with an established set of data and a certain understanding of how that data is classified.

Supervised learning is intended to find patterns in data that can be applied to an analytics process.

This data has labeled features that define the meaning of data. For example, you can create a machine-learning application that distinguishes between millions of animals, based on images and written descriptions.


---
group: "dk"
---
## Neural Net Overview

```python
img_in = Input(shape=(120,160,3), name='img_in')
x = img_in
x = Convolution2D(24, (5,5), strides=(2,2), activation='relu')(x)
x = Convolution2D(32, (5,5), strides=(2,2), activation='relu')(x)
x = Convolution2D(64, (5,5), strides=(2,2), activation='relu')(x)
x = Convolution2D(64, (3,3), strides=(2,2), activation='relu')(x)
x = Convolution2D(64, (3,3), strides=(1,1), activation='relu')(x)

x = Flatten(name='flattened')(x)

x = Dense(100, activation='relu')(x)
x = Dropout(.1)(x)
x = Dense(50, activation='relu')(x)
x = Dropout(.1)(x)

# categorical output of the angle
angle_out = Dense(15, activation='softmax', name='angle_out')(x)

# continuous output of the throttle
throttle_out = Dense(1, activation='softmax', name='throttle_out')(x)
```

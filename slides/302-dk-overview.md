---
group: "dk"
---
## What is a Donkey Car

"Donkey" is a high level self driving library written in Python. It was developed with a focus on enabling fast experimentation and easy contribution.

A "Donkey Car" is an autonomous capable vehicle that utilised the "Donkey" software ecosystem.

=====
#### Why the name Donkey?
The ultimate goal of this project is to build something useful. Donkey's were one of the first domesticated pack animals, they're notoriously stubborn, and they are kid safe. Until the car can navigate from one side of a city to the other, we'll hold off naming it after some celestial being.

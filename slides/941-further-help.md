---
group: "further-help"
---
## Resources

The Donkey Car project.

  * [Website](https://donkeycar.com)
  * [Documentation](https://docs.donkeycar.com)
  * [Code Repository](https://github.com/autorope/donkeycar)

The Ballarat Hackerspace

  * Open Mondays, Tuesdays, Wednesdays and Saturdays.
  * See [website](https://ballarathackerspace.org.au) for details.

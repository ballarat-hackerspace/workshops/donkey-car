---
group: "dk"
---
## Let's Get Driving

1. Boot up the car.
2. SSH to donkey-colour.local
3. python ~/mycar/manage.py drive
4. Browse to donkey-colour.local:8887

<div class="full-image">
  <img src="./img/donkey-car-start-driving.png" />
</div>

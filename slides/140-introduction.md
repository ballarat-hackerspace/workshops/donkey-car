---
layout: ""
---

## Introduction

This workshop will walk you through an introduction of Artificial Intelligence and Machine Learning concepts and provide a practial hands on experience with a scale model self driving car.

You will learn:

  * Foundation elements of Artificial Intelligence and Machine Learning.
  * Foundation elements of the Donkey Car project.
  * How to collect data for machine learning processsing.
  * How to train your own neural net.
  * How to iterate and improve on your neural nets.

---
group: "ai"
---
### Machine Learning

Machine learning (ML) essentially is a building block for AI.

By doing ML, you are teaching a machine to learn how to perform a task, such as:

- image recognition,
- recommender systems, and
- fraud detection.

Common examples of this phenomenon are virtual personal assistants, refined search engine results, image recognition, and product recommendations.

=====
=====

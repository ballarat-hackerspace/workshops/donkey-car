---
group: "ml"
---
## Reinforcement Learning

Reinforcement learning is a behavioral learning model.

The algorithm receives feedback from the data analysis, guiding the user to the best outcome.

Reinforcement learning differs from other types of supervised learning, because the system isn't trained with the sample data set. Rather, the system learns through trial and error. Therefore, a sequence of successful decisions will result in the process being reinforced, because it best solves the problem at hand.


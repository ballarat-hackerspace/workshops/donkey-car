---
group: "ml"
---
## Unsupervised Learning

Used when the problem requires a massive amount of unlabeled data. For example, social media applications, such as Twitter, Instagram and Snapchat, all have large amounts of unlabeled data.

Understanding the meaning behind this data requires algorithms that classify the data based on the patterns or clusters it finds.

Unsupervised learning conducts an iterative process, analysing data without human intervention.

For example, it is used within email spam-detecting technology. There are far too many variables in legitimate and spam emails for an analyst to be able tag unsolicited bulk email. Instead, machine learning classifiers, based on clustering and association, are applied to identify unwanted email.


---
group: "ai"
---
### Artifical Intelligence

Artificial Intelligence (AI) describes machines that can perform tasks resembling those of humans.

> "The ability of machines to work and think, like the human brain."

AI implies machines that artificially model human intelligence.

AI systems help us manage, model, and analyse complex systems.

=====
=====

---
group: "ml"
---
## Machine Learning

Machine Learning is concerned with giving machines the ability to learn by training algorithms on a huge amount of data. It makes use of algorithms and statistical models to perform a task without needing explicit instructions.

There are three main types of ML:
- Supervised and semi-supervised learning
- Unsupervised learning
- Reinforcement learning

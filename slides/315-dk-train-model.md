---
group: "dk"
---
## Train Your Model

1. Copy tub data from the car to the training computer.
2. python ~/mycar/manage.py train --tub &lt;tub folder names comma separated&gt; --model mypilot.h5
3. Copy model back the car.
4. python ~/mycar/manage.py drive --model ~/models/mypilot.h5

---
group: "dk"
---
## Software Architecture

<div class="full-image">
  <img src="./img/donkey-car-software-architecture.png" />
</div>

Run the "vehicle loop" 30 times per second.
